/**
 * Orbstream Client SDK
 *
 * @package Orbstream
 * @license MIT
 */
exports.Orbstream = Orbstream

/**
 * Orbstream
 *
 * @param 	String	baseUrl
 * @param 	String	appId
 * @param 	String	appSecret
 *
 * @return  Orbstream
 */
function Orbstream(baseUrl, appId, appSecret){
	if ( ! this instanceof Orbstream ) {
		return new Orbstream(baseUrl, appId, appSecret)
	}

	var client = require('node-rest-client').Client;
	var client = new client()
	var getUrl = function(endpoint){
		return (baseUrl.replace(/^\/|\/$/g, '') + "/" + endpoint.replace(/^\/|\/$/g, '')).replace(/^\/|\/$/g, '')
	}

	/**
     * Generates a safe unique id
	 *
	 * @param 	Function cb
     *
     * @return Orbstream
     */
	this.uniqid = function(cb){
		client.get(getUrl("/helpers/uniqid"), function(data){
			cb && cb(data.data)
		})
		return this
	}

	/**
     * Generates a safe unique id
	 *
	 * @param 	Object 	 payload
	 * @param 	Function cb
     *
     * @return Orbstream
     */
	this.publish = function(payload, cb){
		args = {
			headers: {
				"Authorization": "Bearer " + appSecret,
				"Content-Type": "application/json"
			},
			data: payload
		}
		client.post(getUrl("/stream/" + appId + "/messages"), args, function(data){
			cb && cb(data.data)
		})
		return this
	}

	/**
     * Generates a safe unique id
	 *
	 * @param 	String 	 channel
	 * @param 	Function cb
     *
     * @return Orbstream
     */
	this.clients = function(channel, cb) {
		args = {
			headers: {
				"Authorization": "Bearer " + appSecret
			}
		}
		client.get(getUrl("/stream/" + appId + "/channels/" + channel + "/clients"), args, function(data){
			cb && cb(data.data)
		})
		return this
	}

	return this
}
