Orbstream Node.js SDK
=========================

> `npm install orbstream`

```javascript
var Orbstream = require("orbstream").Orbstream

var baseUrl = "http://localhost:8080/"
var appId = "01980671c6f7e71af9aadb624847005b41915f4c4425e8cf2a11731b3e0b3fd6f39d75a9c88e22b81bceccb1ab8a30ad34188601114e10fb1fa8b94073829ed7"
var appSecret = "8fd7199214954ffbbd3c7876e05a74d7e0ec57b8f1e468045ae3fdf3a9f82540249890f9b6a29357e9c78873e4d5fb8d2dc2b0fe29494d1a9c021f1827795a74"

var orbstream = new Orbstream(
    baseUrl,
    appId,
    appSecret
)

// uniqid
orbstream.uniqid(id => console.log(id))

// publish message
orbstream.publish({channel: "channel-1", body: "wow"}, data => console.log(data))

// get clients
orbstream.clients("channel1", data => console.log(data))

```
